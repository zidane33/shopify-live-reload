#!/bin/sh
exec <"$0"; read v; read v; exec /usr/bin/osascript - "$@"; 

-- the above is some shell trickery that lets us write the rest of
-- the file in plain applescript

tell application "Chrome" to tell the active tab of its first window
    reload
end tell