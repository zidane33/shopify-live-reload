## Shopify Live Reload

1. Allow reload-script to be executable
```chmod +x reload-script.sh```
2. Add reload-script.sh to your bin folder
```cp reload-script.sh /usr/local/bin```
3. Install fswatch package
```brew install fswatch```
4. When starting themekit, make sure to use the --notify option
```theme watch --notify=temp```
5. In the same directory run fswatch
```fswatch -o temp | xargs -n1 -I{} reload-script.sh```
6. Add temp to .gitignore

Note that this only works for MacOS only. The other limitation that this will only reload the last active tab and only works with Chrome. You can change browsers in the script. 
